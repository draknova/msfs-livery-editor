import logging
import os
from pathlib import Path
from appdirs import user_log_dir, user_cache_dir

from PIL import ImageFont, ImageDraw, Image

_USR_LOG_DIR = Path(user_log_dir("MSFSLivery"))
_USR_LOG_FILE = Path(_USR_LOG_DIR, "msfslivery.log")

_USR_CACHE_DIR = Path(user_cache_dir("MSFSLivery"))
_USR_CACHE_VALUES = Path(_USR_CACHE_DIR, "values.json")


def error_handler(func):
    try:
        return func()
    except Exception as err:
        logger.error(err)
        raise err


def get_logger():
    logger = logging.Logger(__name__)

    stream_handler = logging.StreamHandler()
    if not _USR_LOG_DIR.exists():
        os.makedirs(_USR_LOG_DIR)
    file_handler = logging.FileHandler(_USR_LOG_FILE)

    formatter = logging.Formatter('[%(levelname)s] - %(message)s')
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)

    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)
    return logger


def resolve_path(basepath, match, is_file=True):
    """

    :param basepath:
    :param match:
    :param is_file:
    :return:
    """
    for path in Path(basepath).glob("*"):
        if path.is_file() is is_file and path.name.lower() == match:
            return path
    raise IOError(f"No {'file' if is_file else 'directory'} match {match}")


def generate_thumbnail(text, export_path):
    font_path = Path(__file__, "../font/Russo_One.ttf").as_posix()
    resolution = (1618, 582)
    im = Image.new('RGB', (1618, 582), color='#333')

    draw = ImageDraw.Draw(im)
    font = ImageFont.truetype(font_path, 100)
    scale_x = resolution[0] / font.getsize_multiline(text)[0] * 0.9
    scale_y = resolution[1] / font.getsize_multiline(text)[1] * 0.8
    scale = min(scale_x, scale_y)
    font = ImageFont.truetype(font_path, int(100 * scale))
    draw.text([r/2 for r in resolution], text, font=font, anchor="mm")
    im.save(Path(export_path), "JPEG")


logger = get_logger()
