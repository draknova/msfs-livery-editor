import json
import os
import sys
from appdirs import user_cache_dir
from PySide2 import QtWidgets, QtGui, QtCore

from msfsliveryeditor import livery

_USR_CACHE_DIR = user_cache_dir("MSFSLivery")
_USR_CACHE_VALUES = os.path.join(_USR_CACHE_DIR, "values.json")


class MSFSLiveryWizard(QtWidgets.QWizard):

    execute = QtCore.Signal()

    saved_fields = [
        "livery_dir", "msfs_dir",
        "title", "variation", "version", "creator",
        "airline_name", "airline_icao", "tail_number"
    ]

    def __init__(self):
        super(MSFSLiveryWizard, self).__init__()

        self.setWindowTitle("MSFS Livery Editor (Alpha)")
        self.setWizardStyle(QtWidgets.QWizard.ClassicStyle)

        self.currentIdChanged.connect(self.on_page_changed)

        self.create_pages()
        self.load_values()

    def create_pages(self):
        self.addPage(IntroPage())
        self.addPage(LiveryDirPage())
        self.addPage(MsfsDirPage())
        self.addPage(LiveryPage())
        self.addPage(AtcPage())
        self.addPage(ReviewPage())
        self.addPage(FinalPage())

    def on_page_changed(self, id):
        """Executed when a switching to an other page"""
        if self.page(id - 1) and self.page(id - 1).isCommitPage():
            msfs = livery.MSFSLivery(
                livery_dir=self.field("livery_dir"),
                msfs_dir=self.field("msfs_dir"),
                title=self.field("title"),
                variation=self.field("variation"),
                creator=self.field("creator"),
                version=self.field("version"),
                atc_id=self.field("atc_id"),
                atc_airline=self.field("atc_airline"),
                icao_airline=self.field("icao_airline"),
            )
            msfs.create()

    def on_finished(self, result):
        print("Done")
        if result:
            self.save_values()

    def save_values(self):
        """Saving field values for easy reload."""
        # Get fields values
        values = {}
        for field_name in self.saved_fields:
            value = self.field(field_name)
            if not value:
                continue
            values[field_name] = value

        # Make directories
        if not os.path.isdir(_USR_CACHE_DIR):
            os.makedirs(_USR_CACHE_DIR)

        # Write to cache file
        with open(_USR_CACHE_VALUES, "w") as f:
            json.dump(values, f, indent=4)

        print(_USR_CACHE_VALUES)
        print(values)

    def load_values(self):
        if not os.path.isfile(_USR_CACHE_VALUES):
            return

        with open(_USR_CACHE_VALUES) as f:
            values = json.load(f)

        print(_USR_CACHE_VALUES)

        WizardPage.default_values.update(values)


class WizardPage(QtWidgets.QWizardPage):

    # Store field previous values
    default_values = {}

    def __init__(self):
        super(WizardPage, self).__init__()

        self.layout = QtWidgets.QFormLayout()
        self.setLayout(self.layout)

    def initializePage(self):
        """Set initial field values."""
        for key, value in self.default_values.items():
            if self.field(key) != "":
                continue
            self.setField(key, value)

    def browse_directory_and_set_field(self, field):
        """Browse for existing directory and set field value.

        :param field: Field name to update
        :return:
        """
        dialog = QtWidgets.QFileDialog()
        value = dialog.getExistingDirectory()
        if value:
            self.setField(field, value)
        return value

    @staticmethod
    def label_from_field(field):
        return " ".join([f.capitalize() for f in field.split("_")])

class IntroPage(WizardPage):
    def __init__(self):
        super(IntroPage, self).__init__()
        self.setTitle("Introduction")

        msg = "This wizard is here to easily fill config files for your liveries."
        msg += "\n\nWhat will this tool do?"
        msg += "\n\t- Create folders structure"
        msg += "\n\t- Create textures json files"
        msg += "\n\t- Update manifest.json"
        msg += "\n\t- Update config files"
        msg += "\n\t- Update layout.json"
        msg += "\n\t- Generate default thumbnail"

        label = QtWidgets.QLabel(msg)
        self.layout.addWidget(label)


class LiveryDirPage(WizardPage):
    def __init__(self):
        super(LiveryDirPage, self).__init__()
        self.setTitle("Livery Directory")

        label = QtWidgets.QLabel("Select your livery root directory.")
        self.layout.addWidget(label)

        livery_dir = QtWidgets.QLineEdit()
        self.layout.addWidget(livery_dir)
        self.registerField("livery_dir", livery_dir)

        browser_button = QtWidgets.QPushButton("Browse")
        browser_button.clicked.connect(lambda: self.browse_directory_and_set_field("livery_dir"))
        self.layout.addWidget(browser_button)


class MsfsDirPage(WizardPage):
    def __init__(self):
        super(MsfsDirPage, self).__init__()
        self.setTitle("MSFS Directory")

        label = QtWidgets.QLabel("Select MSFS Aircraft root directory.")
        self.layout.addWidget(label)

        msfs_dir = QtWidgets.QLineEdit()
        self.layout.addWidget(msfs_dir)
        self.registerField("msfs_dir", msfs_dir)

        browser_button = QtWidgets.QPushButton("Browse")
        browser_button.clicked.connect(lambda: self.browse_directory_and_set_field("msfs_dir"))
        self.layout.addWidget(browser_button)


class LiveryPage(WizardPage):
    def __init__(self):
        super(LiveryPage, self).__init__()
        self.setTitle("Livery Settings")

        for field in ["title", "variation", "creator", "version"]:
            widget = QtWidgets.QLineEdit()
            self.layout.addRow(self.label_from_field(field), widget)
            self.registerField(field, widget)


class AtcPage(WizardPage):
    def __init__(self):
        super(AtcPage, self).__init__()
        self.setTitle("ATC Settings")

        for field in ["airline_icao", "airline_name", "tail_number"]:
            widget = QtWidgets.QLineEdit()
            self.layout.addRow(self.label_from_field(field), widget)
            self.registerField(field, widget)


class ReviewPage(WizardPage):
    def __init__(self):
        super(ReviewPage, self).__init__()
        self.setTitle("Review")

        self.label = QtWidgets.QLabel()
        self.layout.addWidget(self.label)

        self.setCommitPage(True)
        self.setButtonText(QtWidgets.QWizard.CommitButton, "Configure Livery")

    def initializePage(self):
        msg = "Livery ready to be configured. Please review the following information:"

        fields = [
            "livery_dir", "msfs_dir", "",
            "title", "variation", "version", "creator", "",
            "airline_name", "airline_icao", "tail_number"
        ]
        for field in fields:
            if not field:
                msg += "\n"
                continue
            msg += "\n\t{:<25}    {}".format(self.label_from_field(field) + ":", self.field(field))
        self.label.setText(msg)


class FinalPage(WizardPage):
    def __init__(self):
        super(FinalPage, self).__init__()
        self.setTitle("Done")

        label = QtWidgets.QLabel("Livery folder updated.")
        self.layout.addWidget(label)


def launch():
    """Launch MSFS Livery Editor"""
    app = QtWidgets.QApplication(sys.argv)
    wizard = MSFSLiveryWizard()
    wizard.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    launch()
