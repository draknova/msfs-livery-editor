import configparser
import json
import os
from pathlib import Path
import shutil

from msfsliveryeditor.utils import logger, resolve_path, generate_thumbnail


class MSFSLivery(object):
    def __init__(self, livery_dir, msfs_dir, title, variation, creator="", version="0.1.0",
                 atc_id="", atc_airline="", icao_airline=""):
        """
        Initialize MSFS Livery.

        :param livery_dir: Path to livery folder to write out
        :param msfs_dir: Path to msfs base aircraft
        :param title: Name of the livery
        :param variation: Livery code name
        :param creator: Livery creator name
        :param version: Livery version number
        :param atc_id: Default tail number
        :param atc_airline: Airline name
        :param icao_airline: Airline ICAO
        """
        self.livery_root = Path(livery_dir)
        self.msfs_root = Path(msfs_dir)
        if not self.msfs_root.is_dir():
            raise IOError(f"{self.msfs_root.absolute().as_posix()} does not exists.")

        self.title = title
        self.variation = variation
        self.creator = creator
        self.version = version
        self.atc_id = atc_id
        self.atc_airline = atc_airline
        self.icao_airline = icao_airline

        # Variable to get from msfs_root
        self.aircraft = ""

        # Config
        self.manifest = configparser.ConfigParser()
        self.aircraft_config = configparser.ConfigParser()
        self.model_config = configparser.ConfigParser()
        self.texture_config = configparser.ConfigParser()
        self.default_manifest = configparser.ConfigParser()
        self.default_aircraft_config = configparser.ConfigParser()
        self.default_model_config = configparser.ConfigParser()
        self.default_texture_config = configparser.ConfigParser()

        # Livery specific files and paths
        self.livery_layout = Path()
        self.livery_manifest = Path()
        self.livery_aircraft_cfg = Path()
        self.livery_texture_cfg = Path()
        self.livery_aircraft_dir = Path()
        self.livery_model_dir = Path()
        self.livery_texture_dir = Path()

        # Msfs specific files and paths
        self.msfs_layout = Path()
        self.msfs_manifest = Path()
        self.msfs_aircraft_cfg = Path()
        self.msfs_texture_cfg = Path()
        self.msfs_aircraft_dir = Path()
        self.msfs_model_dir = Path()
        self.msfs_texture_dir = Path()

        # Initialize variables
        self._init_aircraft()
        self._init_paths()
        self._init_default_manifest()
        self._init_default_aircraft_config()
        self._init_default_model_config()
        self._init_default_texture_config()

    # #######################################################
    # PRIVATE METHODS
    # #######################################################

    def _init_aircraft(self):
        """Find Aircraft name inside base aircraft folder."""
        for path in Path(self.msfs_root).glob("SimObjects/Airplanes/*"):
            if not path.is_dir():
                continue
            self.aircraft = path.name
            break

        if not self.aircraft:
            msg = f"Invalid MSFS Directory: {self.msfs_root}"
            logger.error(msg)
            raise IOError(msg)

        logger.debug(f"Identified aircraft: {self.aircraft}")
        return self.aircraft

    def _init_paths(self):
        """Initialize all the path variables."""
        logger.debug(f"Initialize all the path variables.")
        self.livery_aircraft_dir = Path(self.livery_root, "SimObjects/Airplanes", f"{self.aircraft}-{self.variation}")
        self.livery_model_dir = Path(self.livery_aircraft_dir, f"model.{self.variation}")
        self.livery_texture_dir = Path(self.livery_aircraft_dir, f"TEXTURE.{self.variation}")
        self.livery_layout = Path(self.livery_root, "layout.json")
        self.livery_manifest = Path(self.livery_root, "manifest.json")
        self.livery_aircraft_cfg = Path(self.livery_aircraft_dir, "aircraft.cfg")
        self.livery_model_cfg = Path(self.livery_model_dir, "model.cfg")
        self.livery_texture_cfg = Path(self.livery_texture_dir, "texture.cfg")

        self.msfs_aircraft_dir = Path(self.msfs_root, "SimObjects/Airplanes", self.aircraft)
        self.msfs_model_dir = resolve_path(self.msfs_aircraft_dir, "model", is_file=False)
        self.msfs_texture_dir = resolve_path(self.msfs_aircraft_dir, "texture", is_file=False)
        self.msfs_layout = Path(self.msfs_root, "layout.json")
        self.msfs_manifest = Path(self.msfs_root, "manifest.json")
        self.msfs_aircraft_cfg = resolve_path(self.msfs_aircraft_dir, "aircraft.cfg")
        self.msfs_model_cfg = resolve_path(self.msfs_model_dir, "model.cfg")
        self.msfs_texture_cfg = resolve_path(self.msfs_texture_dir, "texture.cfg")

    def _init_default_manifest(self):
        """Loading manifest config from base aircraft."""
        logger.debug(f"Loading manifest config from base aircraft.")
        with open(self.msfs_manifest) as f:
            self.default_manifest = json.load(f)

        for key in ["DRMProtection", "total_package_size"]:
            if key not in self.default_manifest.keys():
                continue
            self.default_manifest.pop(key)
        return self.default_manifest

    def _init_default_aircraft_config(self):
        """Loading aircraft config from base aircraft."""
        logger.debug(f"Loading aircraft config from base aircraft.")
        self.default_aircraft_config.read(self.msfs_aircraft_cfg.absolute().as_posix())

    def _init_default_model_config(self):
        """Loading model config from base aircraft."""
        logger.debug(f"Loading model config from base aircraft.")
        self.default_model_config.read(self.msfs_model_cfg.absolute().as_posix())

    def _init_default_texture_config(self):
        """Loading texture config from base aircraft."""
        logger.debug(f"Loading texture config from base aircraft.")
        self.default_texture_config.read(self.msfs_texture_cfg.absolute().as_posix())

    # #######################################################
    # PUBLIC METHODS
    # #######################################################

    def create(self):
        # Build folder hierarchy
        self.build_folders()

        # Create/Write various files
        self.write_manifest()
        self.write_livery_thumbnail()
        self.write_aircraft_config()
        self.write_model_config()
        self.write_texture_config()
        self.write_texture_stats()

        # Finally write layout
        self.write_layout()
        logger.info("Finished livery configuration !")

    def build_folders(self):
        for path in [self.livery_model_dir, self.livery_texture_dir]:
            if path.exists():
                logger.debug(f"Folder already exists: {path}")
                continue

            logger.info(f"Creating folder: {path}")
            try:
                os.makedirs(path.absolute().as_posix())
            except IOError as err:
                logger.error(err)
                raise err

    def write_layout(self):
        """Write layout.json file."""
        layout_content = {"content": []}

        # Loop over every files
        for file in Path(self.livery_root).glob("**/*.*"):
            if file.is_dir():
                continue

            # Skip few files
            if file in [Path(self.livery_root, f) for f in ["manifest.json", "layout.json"]]:
                continue

            # Add info to layout.json
            layout_content["content"].append({
                "path": file.relative_to(self.livery_root).as_posix(),
                "size": int(file.stat().st_size),
                "date": int(file.stat().st_ctime)
            })

        logger.info(f"Writing layout: {self.livery_layout}")
        with open(self.livery_layout, "w") as f:
            json.dump(layout_content, f, indent=4)

    def write_manifest(self):
        livery_manifest = self.default_manifest.copy()
        livery_manifest["dependencies"] = []
        livery_manifest["content_type"] = "LIVERY"
        livery_manifest["title"] = " - ".join([self.default_manifest["title"], self.title])
        livery_manifest["creator"] = self.creator
        livery_manifest["package_version"] = self.version

        logger.info(f"Writing manifest: {self.livery_manifest}")
        with open(self.livery_manifest, "w") as f:
            json.dump(livery_manifest, f, indent=4)

    def write_aircraft_config(self):
        aircraft_config = configparser.ConfigParser()
        aircraft_config.read_dict(self.default_aircraft_config)

        # Remove un-necessary Sections
        for section in aircraft_config.sections():
            if section in ["VERSION", "VARIATION", "FLTSIM.0"]:
                continue
            aircraft_config.remove_section(section)

        # Version Section
        aircraft_config["VERSION"]["major"] = "1"
        aircraft_config["VERSION"]["minor"] = "0"

        # Variation Section
        if not aircraft_config.has_section("VARIATION"):
            aircraft_config.add_section("VARIATION")
        aircraft_config["VARIATION"]["base_container"] = f"\"..\\{self.aircraft}\""

        fltsim_dict = dict(aircraft_config["FLTSIM.0"].items())
        aircraft_config.remove_section("FLTSIM.0")
        aircraft_config.add_section("FLTSIM.0")
        for key, value in fltsim_dict.items():
            aircraft_config["FLTSIM.0"][key] = value

        # FLTSIM Section
        title = self.default_aircraft_config["FLTSIM.0"]["title"].strip("\"")
        title = f"{title} {self.title}"
        aircraft_config["FLTSIM.0"]["title"] = f"\"{title}\""
        aircraft_config["FLTSIM.0"]["model"] = f"\"{self.variation}\""
        aircraft_config["FLTSIM.0"]["texture"] = f"\"{self.variation}\""
        aircraft_config["FLTSIM.0"]["ui_type"] = f"\"{self.title}\""
        aircraft_config["FLTSIM.0"]["ui_variation"] = f"\"{self.variation}\""
        aircraft_config["FLTSIM.0"]["ui_createdby"] = f"\"{self.creator}\""
        aircraft_config["FLTSIM.0"]["atc_id"] = f"\"{self.atc_id}\""
        aircraft_config["FLTSIM.0"]["atc_airline"] = f"\"{self.atc_airline}\""
        aircraft_config["FLTSIM.0"]["icao_airline"] = f"\"{self.icao_airline}\""

        logger.info(f"Writing aircraft.cfg: {self.livery_aircraft_cfg}")
        with open(self.livery_aircraft_cfg, "w") as f:
            aircraft_config.write(f)

    def write_texture_config(self):
        texture_config = configparser.ConfigParser()
        texture_config.read_dict(self.default_texture_config)
        fallback_keys = [k for k in texture_config["fltsim"].keys() if k.startswith("fallback")]
        max_index = max([int(k.split(".")[-1]) for k in fallback_keys if k.split(".")[-1].isdigit()])

        # Add fallback to msfs texture
        texture_dir_relpath = self.msfs_texture_dir.relative_to(self.msfs_aircraft_dir.parent)
        texture_config["fltsim"][f"fallback.{max_index + 1}"] = f"..\\..\\{texture_dir_relpath}"
        texture_config["fltsim"][f"fallback.{max_index + 2}"] = f"..\\texture"

        logger.info(f"Writing texture.cfg: {self.livery_texture_cfg}")
        with open(self.livery_texture_cfg, "w") as f:
            texture_config.write(f)

    def write_model_config(self):
        model_config = configparser.ConfigParser()
        model_config.read_dict(self.default_model_config)

        # Add fallback to msfs texture
        texture_dir_relpath = self.msfs_model_dir.relative_to(self.msfs_aircraft_dir.parent)
        for key in ["exterior", "interior", "normal"]:
            try:
                file = model_config["models"][key].split("\\")[-1]
                model_config["models"][key] = f"..\\..\\{texture_dir_relpath}\\{file}"
            except KeyError:
                continue

        logger.info(f"Writing model.cfg: {self.livery_model_cfg}")
        with open(self.livery_model_cfg, "w") as f:
            model_config.write(f)

    def write_texture_stats(self):
        for file in self.livery_texture_dir.glob("*"):
            if not file.is_file or file.suffix.lower() != ".dds":
                continue

            texture_stats = {
                "Version": 1,
                "SourceFileDate": int(file.stat().st_ctime),
                "Flags": ["FL_BITMAP_COMPRESSION", "FL_BITMAP_MIPMAP"],
            }

            filepath = Path(file.parent, f"{file.name}.json")
            logger.info(f"Writing texture stats: {filepath}")
            with open(filepath, "w") as f:
                json.dump(texture_stats, f)

    def write_livery_thumbnail(self):
        """Import thumbnail image from original aircraft."""
        thumbnail_name = "thumbnail.jpg"
        try:
            resolve_path(self.livery_texture_dir, thumbnail_name)
        except IOError:
            livery_thumbnail_path = Path(self.livery_texture_dir, thumbnail_name)
            logger.info(f"Writing thumbnail image: {livery_thumbnail_path}")
            generate_thumbnail(self.title, livery_thumbnail_path)
