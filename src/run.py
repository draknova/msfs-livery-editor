import argparse

from msfsliveryeditor.livery import MSFSLivery

def get_args():
    parser = argparse.ArgumentParser(description="Tool to simplify livery creation for MSFS 2020")
    parser.add_argument("livery_dir", help="Path to livery directory")
    parser.add_argument("msfs_dir", help="Path to msfs aircraft directory")
    parser.add_argument("title", help="Livery name")
    parser.add_argument("variation", help="Livery code")
    parser.add_argument("--creator", help="Name of the creator")
    parser.add_argument("--version", help="Version number")
    parser.add_argument("--atc_id", help="ATC tail number")
    parser.add_argument("--atc_airline", help="ATC airline name")
    parser.add_argument("--icao_airline", help="ATC airline icao")
    return parser.parse_args()


def main(args):
    livery = MSFSLivery(
        livery_dir=args.livery_dir,
        msfs_dir=args.msfs_dir,
        title=args.title,
        variation=args.variation,
        creator=args.creator,
        version=args.version,
        atc_id=args.atc_id,
        atc_airline=args.atc_airline,
        icao_airline=args.icao_airline
    )
    livery.create()


if __name__ == "__main__":
    main(get_args())