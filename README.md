# MSFS Livery Editor

Simple tool to easily create liveries for MSFS 2020

## Usage
Execute with Python the file `run_gui.py`. This should open the following window. Simply click the ***Next*** button.

![intro_window](docs/images/intro.png)

Select your livery root folder.

This is the folder where the directory structure and files will be created.   

![livery_window](docs/images/livery_dir.png)

Select the MSFS Aircraft root folder.

This is the folder of the aircraft you want to create the liveries for. This should usually be: 

`C:\Users\[username]\AppData\Local\Packages\Microsoft.FlightSimulator_8wekyb3d8bbwe\LocalCache\Packages\Official\[aircraft-package]`

![msfs_window](docs/images/msfs_dir.png)

Enter any information regarding your livery.
- Title: Name of your livery. Displayed in MSFS Livery screen
- Variation: Livery Code/ICAO, must be unique to the livery or it will conflict with other livery
- Creator: Your name
- Version: Current livery version

![package_window](docs/images/package.png)

Enter optional livery ATC customization.

![atc_window](docs/images/atc.png)

Review modification that will be done to the package, then press ***Configure Livery***

![review_window](docs/images/review.png)

If installation failed, report will be displayed here.

![finish_window](docs/images/Finish.png)

## Requierement
This is currently only a Python 3 script. Please download python3 from here:

https://www.python.org/download/releases/3.0/

Other requierement to install with pip:
```
pip install appdirs
pip install pillow
pip install pyside2
```